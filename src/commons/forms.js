let forms = {
    "stages": [
        {"title": "Stage Name", "type":"text", "field": "name"},
        {"title": "Stage Color", "type":"colorpicker", "field": "color"},
    ],
    "step": [
        {"title": "Step Name", "type":"text", "field": "name"},
        {"title": "Step Color", "type": "colorpicker", "field": "color"},
    ],
    "touchpoint": [
        {"title": "Touchpoint Name", "type":"text", "field": "name"},
        {"title": "Touchpoint Color", "type": "colorpicker", "field": "color"},
    ],
    "team": [
        {"title": "Team Name", "type": "text", "field": "name"},
    ],
}

export default forms