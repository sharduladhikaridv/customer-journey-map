import Vue from 'vue'
import vuex from 'vuex'
Vue.use(vuex)

import { mapGetters,mapActions } from "vuex"

export const store = new vuex.Store({
    state: {
        stepStore:[],
        methods: {
            ...mapActions([""])
        },
        computed: mapGetters([""]),
        created() {
            this.fetch();
        },
    },
    actions: {
        addStep(context, payload) {
            context.commit('UPDATE_STEP', payload)
        },
        stepStore(context,payload) {
            // this.stepStore.commit({ dispatch, commit })
            // {
            //     return dispatch('stepStore').then(() => {
            //         commit('mutations')
            //     })
            // }
                context.commit('UPDATE_STEP',payload)
            }
        },
        
    mutations: {
        // setStep: (state, step) => (step.state = step)

        UPDATE_STEP(state,payload){
            state.stepStore = payload
        }
    },
})

// //Initial state
// const state = () => ({
//     all:[]
// });

// //Getters
// const getters = {}

// //Actions
// const actions = {
//     async stepStore ({ commit }) {
//         const stepStore = await step.stepStore()
//         commit('stepStore', steps)
//     },
// }

// //Mutations
// const mutations = {
//     stepStore (state, steps) {
//         state.all = steps
//     },

//     stepStore (state, {content}) {
//         const step = state.all.find(step => step.content === content)
//         step.stepStore--
//     },
// }

// export default {
//     state,
//     getters,
//     actions,
//     mutations
// }

