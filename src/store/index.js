import Vue from 'vue'
import Vuex from 'vuex'
// import steps from './modules/step.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: 'Awareness',
    color: '#3EC28F',
    order: [] // will store order here
  },
  getters: {

  },
  actions: {
    addToOrder(context, order) {
      context.commit('UPDATE_ORDER', order)
    }
  },
  mutations: {
    
  },
  // modules:{
  //   steps,
  // }
})









// export default new Vuex.Store({
//   state: {
//   },
//   getters: {
//   },
//   mutations: {
//   },
//   actions: {
//   },
//   modules: {
//   }
// })

// export default createStore({
//   state: {

//   },
//   mutations: {

//   },
//   actions: {

//   },
//   getters: {

//   },
// })
