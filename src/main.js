import Vue from 'vue'
import App from './App.vue'

// Import Draggable files
import draggable from 'vuedraggable'

// Using Draggable Packages
Vue.use(draggable)

// Buefy Icon 
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import { library } from '@fortawesome/fontawesome-svg-core';
// internal icons
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(fas);
Vue.component('vue-fontawesome', FontAwesomeIcon);


Vue.use(Buefy, {
  defaultIconComponent: "vue-fontawesome",
  defaultIconPack: "fas",
  customIconPacks: {
    fas: {
      sizes: {
        default: "lg",
        "is-small": "1x",
        "is-medium": "2x",
        "is-large": "3x"
      },
      iconPrefix: ""
    }
  }
});

// Buefy Icons
Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});

// Vue BUS
import VueBus from 'vue-bus'

Vue.use(VueBus)

//Vuex
// import { createStore } from 'vue'
// import store from './store'
//createApp(App).use(store).mount('#app')

// import Vuex from 'vuex'
// Vue.use(Vuex)

//Custom Prototypes
import forms from "./commons/forms"
import store from './store'
import vuetify from './plugins/vuetify'

Vue.prototype.$forms = forms


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  vuetify,
  store:store
}).$mount('#app')
